# docker build -t react-native-website:latest
# docker run -d -p 3000:3000 react-native-website:latest
FROM bitnami/git:2 AS source
RUN git clone --single-branch https://github.com/facebook/react-native-website /react-native-website

FROM node:16
LABEL maintainer="Justin Firsching <firschj@sunypoly.edu>"

USER node
COPY --from=source --chown=node react-native-website /react-native-website

RUN cd /react-native-website && \
    yarn

EXPOSE 3000
WORKDIR /react-native-website/website
ENTRYPOINT ["yarn", "start"]
CMD ["--host", "0.0.0.0"]
